#!/usr/bin/env python
# -*- coding:utf-8 -*-

# スイッチ、LEDをインポート
import sw
import led
import time

# ボードに接続する
BOAD_PIN_15 = 22 # GPIO22
BOAD_PIN_16 = 23 # GPIO23

BOAD_PIN_18 = 24 # GPIO24
BOAD_PIN_22 = 25 # GPIO25

# Sw 初期化
sw1 = sw.PushSw( BOAD_PIN_15 )
sw2 = sw.PushSw( BOAD_PIN_16 )

# Led 初期化
led1 = led.Led( BOAD_PIN_18 )
led2 = led.Led( BOAD_PIN_22 )

try:
    while True:

        if not sw1.is_status() :
            led1.toggle()

        if not sw2.is_status() :
            led2.toggle()

        time.sleep(0.5)

except KeyboardInterrupt:
    print("detect key interrupt\n")
