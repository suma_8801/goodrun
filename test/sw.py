#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 押しボタンクラス

import RPi.GPIO as GPIO
import time

class PushSw:
    """
    GPIO のピン番号をしていしてインスタンスを生成する
    """

    def __init__( self , pnum  ):

        self.myport = pnum
        GPIO.setmode( GPIO.BCM )
        #GPIO.setmode(GPIO.BOARD) ボード上の番号
        GPIO.setup( self.myport, GPIO.IN )

    def wait_on( self ) :
        """ ON になるのを待つ """
        while GPIO.input( self.myport ) :
            time.sleep( 0.002 )

    def wait_off( self ) :
        """ OFF になるのを待つ """
        while not GPIO.input( self.myport ) :
            time.sleep( 0.002 )

    def is_status( self ) :
        """ ステータスを返す """
        return not GPIO.input( self.myport )
