#!/usr/bin/env python
# -*- coding:utf-8 -*-

# LED を点灯するクラス

import RPi.GPIO as GPIO

class Led:
    """
    GPIO のピン番号をしていしてインスタンスを生成する
    """

    def __init__( self , pnum  ):
        self.myport = pnum	# ポート番号
        self.status = "off"	# ステータス

        GPIO.setmode( GPIO.BCM )
        #GPIO.setmode(GPIO.BOARD) ボード上の番号
        GPIO.setup( self.myport, GPIO.OUT)

    def on( self ) :
        """ オンにする """
        GPIO.output( self.myport, True )
        self.status = "on"

    def off( self ) :
        """ オフにする """
        GPIO.output( self.myport, False )
        self.status = "off"

    def toggle( self ) :
        """ オンオフ反転する """
        if self.status == "off" :
            GPIO.output( self.myport, True )
            self.status = "on"
        else :
            GPIO.output( self.myport, False )
            self.status = "off"

    def is_status( self ) :
        """ ステータスを返す """
        return self.status
