#!/usr/bin/env python
# -*- coding:utf-8 -*-

# i2c の加速度センサーの値を読むテスト


# ちょっと休憩する時間(秒)
INTERVAL  = 0.005

# 最大カウント数
# INTERVAL * MAX_COUNT が計測時間となる
MAX_COUNT = 2000

# デバイスの ID
DEVICE_ID	= 0x33

# 各種インポート
import smbus
import time
import datetime

# i2c のバス番号 固定的に１かな
bus_number = 1

# 加速度センサーの i2c アドレス ic2 機器で決まっている
addr       = 0x18

# 加速度センサー LIS3DH のレジスタ定義
WHO_AM_I   = 0x0f   # これで接続確認できる

# データレート、Lowパワーモード、3軸イネーブル
CTRL_REG1  = 0x20   # センス周波数と 3軸のイネーブル
	DATA_RATE_1Hz   = 0x10
	DATA_RATE_10Hz  = 0x20
	DATA_RATE_25Hz  = 0x30
	DATA_RATE_50Hz  = 0x40
	DATA_RATE_100Hz = 0x50
	DATA_RATE_200Hz = 0x60
	DATA_RATE_400Hz = 0x70
	ENABLE_Z_AXIS   = 0x04
	ENABLE_Y_AXIS   = 0x02
	ENABLE_X_AXIS   = 0x01
	ENABLE_XYZ		= 0x07


CTRL_REG2  = 0x21   # ハイパスフィルター
CTRL_REG3  = 0x22   # INT1の設定
CTRL_REG4  = 0x23   # エンディアン,フルレンジ
	FULL_SCALE_2G   = 0x00
	FULL_SCALE_4G   = 0x10
	FULL_SCALE_8G   = 0x20
	FULL_SCALE_16G  = 0x30
	HIGH_RESOLUTION = 0x08

OUT_X_L    = 0x28   # データ
OUT_X_H    = 0x29

OUT_Y_L    = 0x2A
OUT_Y_H    = 0x2B

OUT_Z_L    = 0x2C
OUT_Z_H    = 0x2D

def get_axis_data( bus , reg_addr ) :
	"""
	bus インスタンスとレジスタアドレスを指定してデータを取得する。
	戻り値は、符号付き整数
	レジスタアドレスの指定は、下位アドレスだけで良い。
	"""
	# データ(ノーマルモードの時は、L は常に 0x00 らしい)
	dat_l = bus.read_i2c_block_data( addr, reg_addr     , 1 )
	dat_h = bus.read_i2c_block_data( addr, reg_addr + 1 , 1 )

	a_data = ( dat_h[0] << 8 ) | dat_l[0]
	if 0x8000 & a_data :
		a_data = ( a_data + 1 ) ^ 0xffff * -1

	return a_data


# メイン処理はここから
if __name__ == '__main__':

	#  bus インスタンス
	bus = smbus.SMBus(bus_number)

	# 0x33 でないとおかしい、接続チェックに使える
	res = bus.read_i2c_block_data( addr, WHO_AM_I , 1 )

	if res[0] != DEVICE_ID :
		print "No Connection to LIS3DH"
		exit()

	# センサーの初期設定
	bus.write_i2c_block_data( addr, CTRL_REG1 , [ DATA_RATE_400Hz | ENABLE_XYZ ]) # 400Hz , X,Y,Z 有効
	bus.write_i2c_block_data( addr, CTRL_REG4 , [ FULLSCALE_4G ]) # 4G,ノーマル,

	count = 1
	while True :

		# ３軸データを取得
		dat_x = get_axis_data( bus , OUT_X_L )
		dat_y = get_axis_data( bus , OUT_Y_L )
		dat_z = get_axis_data( bus , OUT_Z_L )

		now = datetime.datetime.now()
		print "%d " % count ,
		print now.strftime( "%H:%M:%S.%f" ),
		print "%d %d %d" % ( dat_x , dat_y , dat_z )

		# ちょっと休憩
		time.sleep( INTERVAL )
		count += 1

		if count > MAX_COUNT :
			break
