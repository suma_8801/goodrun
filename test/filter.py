#!/usr/bin/env python
# -*- coding:utf-8 -*-

# gnuplot などでグラフ化するためにデータを修正するフィルタ

import sys
import math

#引数
argv = sys.argv

if len( argv ) < 1 :
	print 'Usage: com filename'
	exit()

# ファイル名を指定して処理をする
for line in open( argv[1] , 'r' ) :
	# 最後の改行は削除して スペース文字でフィールドを分ける
	item = line[:-1].split( ' ' )

	# 番号、時間の印字 , で改行しない
	print item[0],
	print item[1],

	# 16bit で、４Gのレンジなので、実際のGに計算し直す
	# sqrt( x^2 + y^2 ) 
	v = math.sqrt(  int(item[2]) ** 2 +  int(item[3]) ** 2 ) / 32767 * 4
	print v
